﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestionPatrimoine
{
    class Bank
    {
        private Dictionary<LoanDuration, Dictionary<InterestRateQuality, InterestRate>> bankInterestRates;
        private List<Loan> loanRecords = new List<Loan>();

        public Bank(Dictionary<LoanDuration, Dictionary<InterestRateQuality, InterestRate>> _bankInterestRates)
        {
            bankInterestRates = _bankInterestRates;
        }
        public Loan InitiateNewLoan(Borrower _borrower, double _amount, LoanDuration _duration, InterestRateQuality _interestRateQuality, LoanInsurance _insurance)
        {
            InterestRate interestRate = bankInterestRates[FindClosestLoanDuration(_duration)][_interestRateQuality];
            Loan newLoan = new Loan(_borrower, _amount, _duration, interestRate, _insurance);
            loanRecords.Add(newLoan);
            return newLoan;
        }

        private LoanDuration FindClosestLoanDuration(LoanDuration _duration)
        {
            LoanDuration closestLoanDuration;

            if (bankInterestRates.Keys.Any(anticipatedLoanDuration => anticipatedLoanDuration.ValueInYears == _duration.ValueInYears))
            {
                closestLoanDuration = bankInterestRates.Keys.First(anticipatedLoanDuration => anticipatedLoanDuration.ValueInYears == _duration.ValueInYears);
            }
            else
            {
                closestLoanDuration = FindClosestUpperLoanDuration(_duration);
            }

            return closestLoanDuration;
        }
        private LoanDuration FindClosestUpperLoanDuration(LoanDuration _duration)
        {
            LoanDuration upperLoanDurationWithSmallestDifference = new LoanDuration(0);
            foreach (LoanDuration anticipatedLoanDuration in bankInterestRates.Keys)
            {
                if (anticipatedLoanDuration.ValueInYears - _duration.ValueInYears > 0 
                    && anticipatedLoanDuration.ValueInYears - _duration.ValueInYears < upperLoanDurationWithSmallestDifference.ValueInYears)
                {
                    upperLoanDurationWithSmallestDifference = anticipatedLoanDuration;
                }
            }

            return upperLoanDurationWithSmallestDifference;
        }
    }
}
