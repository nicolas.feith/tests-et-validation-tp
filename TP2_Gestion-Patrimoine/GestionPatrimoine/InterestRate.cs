﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestionPatrimoine
{
    class InterestRate
    {
        public double Value => valueInPercent / 100;
        public double ValueInPercent => valueInPercent;

        private double valueInPercent;

        public InterestRate(double _valueInPercent)
        {
            valueInPercent = RoundingMethod(_valueInPercent);
        }
        public void AddInterestRate(InterestRate _rateToAdd)
        {
            valueInPercent = RoundingMethod(valueInPercent + _rateToAdd.valueInPercent);
        }
        private static double RoundingMethod(double _percentage)
        {
            double percentage;

            if (_percentage > 100.00d)
            {
                percentage = 100.00d;
            }
            else if (_percentage < -100.00d)
            {
                percentage = -100.00d;
            }
            else
            {
                percentage = Math.Round(_percentage, 2, MidpointRounding.AwayFromZero); ;
            }

            return percentage;
        }

    }
}
