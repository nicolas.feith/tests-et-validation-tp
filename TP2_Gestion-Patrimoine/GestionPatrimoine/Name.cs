﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace GestionPatrimoine
{
    class Name
    {
        public string Value => value;

        private static readonly string C_NAME_REGEX = @"^[\p{L} \.\-]+$";
        private string value;

        public Name(string _value)
        {
            string name = _value.Trim();
            if (!Regex.IsMatch(name, C_NAME_REGEX))
            {
                throw new ArgumentException(value.ToString() + " is not a valid name");
            }
            value = name;
        }
    }
}
