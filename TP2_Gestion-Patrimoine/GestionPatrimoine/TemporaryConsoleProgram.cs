﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestionPatrimoine
{
    class TemporaryProgramWithDebugOutput
    {
        public static void ConsoleTestDisplay()
        {
            Dictionary<AttributesAndHabits, InterestRate> ratesForLoanInsuranceA = new Dictionary<AttributesAndHabits, InterestRate>
            {
                { AttributesAndHabits.ATHLETIC, new InterestRate(-0.05) },
                { AttributesAndHabits.HEARTCONDITION, new InterestRate(0.3) },
                { AttributesAndHabits.ITENGINEER, new InterestRate(-0.05) },
                { AttributesAndHabits.JETFIGHTERPILOT, new InterestRate(0.15) },
                { AttributesAndHabits.SMOKER, new InterestRate(0.15) }
            };
            LoanInsurance loanInsuranceA = new LoanInsurance(ratesForLoanInsuranceA);
            Debug.WriteLine("Taux pratiqués par l'asurrance :");
            foreach(AttributesAndHabits attribute in ratesForLoanInsuranceA.Keys)
            {
                Debug.WriteLine(attribute + "  " + ratesForLoanInsuranceA[attribute].ValueInPercent);
            }

            double[,] interestRatesMatrixBankA =
            {
                { 0.62, 0.43, 0.35 },
                { 0.67, 0.55, 0.45 },
                { 0.85, 0.73, 0.58 },
                { 1.04, 0.91, 0.73 },
                { 1.27, 1.15, 0.89 }
            };
            uint[] loanDurationsMatrixBankA =
            {
                7, 10, 15, 20, 25
            };
            Dictionary<LoanDuration, Dictionary<InterestRateQuality, InterestRate>> bankInterestRatesForBankA = new Dictionary<LoanDuration, Dictionary<InterestRateQuality, InterestRate>>();
            for (int i = 0; i < 5; i++)
            {
                Dictionary<InterestRateQuality, InterestRate> tempDict = new Dictionary<InterestRateQuality, InterestRate>();
                for (int j = 0; j < 3; j++)
                {
                    switch (j)
                    {
                        case 0:
                            tempDict.Add(InterestRateQuality.GOOD, new InterestRate(interestRatesMatrixBankA[i, j]));
                            break;
                        case 1:
                            tempDict.Add(InterestRateQuality.GREAT, new InterestRate(interestRatesMatrixBankA[i, j]));
                            break;
                        case 2:
                            tempDict.Add(InterestRateQuality.EXCELLENT, new InterestRate(interestRatesMatrixBankA[i, j]));
                            break;
                    }  
                }
                bankInterestRatesForBankA.Add(new LoanDuration(loanDurationsMatrixBankA[i]), tempDict);
            }
            Debug.WriteLine("");
            Debug.WriteLine("Intérêt pratiqués :");
            Debug.WriteLine("\tGOOD\tGREAT\tEXCELLENT");
            foreach(LoanDuration line in bankInterestRatesForBankA.Keys)
            {
                Debug.WriteLine(line.ValueInYears + "ans\t" + bankInterestRatesForBankA[line].ElementAt(0).Value.ValueInPercent + "\t" + bankInterestRatesForBankA[line].ElementAt(1).Value.ValueInPercent + "\t" + bankInterestRatesForBankA[line].ElementAt(2).Value.ValueInPercent);
            }
            Bank bankA = new Bank(bankInterestRatesForBankA);

            Borrower borrowerA = new Borrower(new Name("Ingénieur"), new Name("Informatique"), new List<AttributesAndHabits>
            {
                AttributesAndHabits.ITENGINEER, AttributesAndHabits.SMOKER, AttributesAndHabits.HEARTCONDITION
            });
            Borrower borrowerB = new Borrower(new Name("Pilote"), new Name("Davion"), new List<AttributesAndHabits>
            {
                AttributesAndHabits.JETFIGHTERPILOT, AttributesAndHabits.ATHLETIC
            });

            Loan loanA = bankA.InitiateNewLoan(borrowerA, 175000d, new LoanDuration(25), InterestRateQuality.GOOD, loanInsuranceA);
            Loan loanB = bankA.InitiateNewLoan(borrowerB, 200000d, new LoanDuration(15), InterestRateQuality.GREAT, loanInsuranceA);

            Debug.WriteLine("");
            Debug.WriteLine("Prêt de \"" + borrowerA.FirstName.Value + " " + borrowerA.LastName.Value + "\" :");
            Debug.WriteLine("Montant: 175000€\tDurée: 25 ans\tTaux: Bon");
            Debug.WriteLine("Prix global de la mensualité: " + loanA.ComputeMonthlyPayment());
            Debug.WriteLine("Montant de la cotisation mensuelle d'assurance: " + loanA.ComputeMonthlyInsurancePayment());
            Debug.WriteLine("Montant total des intérêts remboursés: " + loanA.ComputeTotalInterestPayment());
            Debug.WriteLine("Montant total de l'assurance: " + loanA.ComputeTotalInsurancePayment());
            Debug.WriteLine("Montant du capital remboursé après 10 ans: " + (loanA.ComputeMonthlyPayment() - loanA.ComputeMonthlyInsurancePayment() - loanA.ComputeMonthlyInterestPayment()) * 10 * 12);

            Debug.WriteLine("");
            Debug.WriteLine("Prêt de \"" + borrowerB.FirstName.Value + " " + borrowerB.LastName.Value + "\" :");
            Debug.WriteLine("Montant: 200000€\tDurée: 15 ans\tTaux: Très Bon");
            Debug.WriteLine("Prix global de la mensualité: " + loanB.ComputeMonthlyPayment());
            Debug.WriteLine("Montant de la cotisation mensuelle d'assurance: " + loanB.ComputeMonthlyInsurancePayment());
            Debug.WriteLine("Montant total des intérêts remboursés: " + loanB.ComputeTotalInterestPayment());
            Debug.WriteLine("Montant total de l'assurance: " + loanB.ComputeTotalInsurancePayment());
            Debug.WriteLine("Montant du capital remboursé après 10 ans: " + (loanB.ComputeMonthlyPayment() - loanB.ComputeMonthlyInsurancePayment() - loanB.ComputeMonthlyInterestPayment()) * 10 * 12);
        }
    }
}
