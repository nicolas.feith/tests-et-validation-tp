﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestionPatrimoine
{
    class Loan
    {
        private readonly double c_initial_amount;
        private double amountLeftToPay;
        private Borrower borrower;
        private LoanDuration duration;
        private InterestRate interestRate;
        private LoanInsurance insurance;

        public Loan(Borrower _borrower, double _amount, LoanDuration _duration, InterestRate _interestRate, LoanInsurance _insurance)
        {
            c_initial_amount = _amount;
            amountLeftToPay = _amount;
            borrower = _borrower;
            duration = _duration;
            interestRate = _interestRate;
            insurance = _insurance;
        }
        public double ComputeMonthlyPayment()
        {
            /*
             * Par étude du site economie.gouv.fr, et par des raccourcis mathématiques, 
             * je suis arrivé à la conclusion (sans certitude) que le Taux Annuel (ou TAEG)
             * est, dans notre exemple, égal à l'addition du taux d'intérêt nominal et du 
             * taux de l'assurance.
             */
            double annualRate = interestRate.Value + insurance.ComputeInsuranceRate(borrower.Attributes).Value;
            double monthlyPayment = (c_initial_amount * ((annualRate)/12))
                / (1 - Math.Pow(1 + ((annualRate)/12), - Convert.ToDouble(duration.ValueInMonths)));

            return monthlyPayment;
        }
        public double ComputeMonthlyInterestPayment()
        {
            return (c_initial_amount / 12) * interestRate.Value;
        }
        public double ComputeMonthlyInsurancePayment()
        {
            return (c_initial_amount / 12) * insurance.ComputeInsuranceRate(borrower.Attributes).Value;
        }
        public double ComputeTotalInterestPayment()
        {
            return c_initial_amount * interestRate.Value * duration.ValueInYears;
        }
        public double ComputeTotalInsurancePayment()
        {
            return c_initial_amount * insurance.ComputeInsuranceRate(borrower.Attributes).Value * duration.ValueInYears;
        }
    }
}
