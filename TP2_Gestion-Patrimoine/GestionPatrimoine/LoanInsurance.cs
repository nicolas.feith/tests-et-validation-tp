﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestionPatrimoine
{
    class LoanInsurance
    {
        private InterestRate defaultInsuranceRate = new InterestRate(0.3d);
        private Dictionary<AttributesAndHabits, InterestRate> criteriasTakenIntoAccountForInsuranceRate;

        public LoanInsurance(Dictionary<AttributesAndHabits, InterestRate> _criteriasTakenIntoAccountForInsuranceRate)
        {
            criteriasTakenIntoAccountForInsuranceRate = _criteriasTakenIntoAccountForInsuranceRate;
        }
        public InterestRate ComputeInsuranceRate(List<AttributesAndHabits> _attributesAndHabits)
        {
            InterestRate insuranceRate = new InterestRate(defaultInsuranceRate.ValueInPercent);
            foreach(AttributesAndHabits attribute in _attributesAndHabits)
            {
                AddAttributeRateToInsuranceRate(insuranceRate, attribute);
            }
            return insuranceRate;
        }

        private void AddAttributeRateToInsuranceRate(InterestRate _insuranceRate, AttributesAndHabits _attribute)
        {
            if (criteriasTakenIntoAccountForInsuranceRate.ContainsKey(_attribute))
            {
                _insuranceRate.AddInterestRate(criteriasTakenIntoAccountForInsuranceRate[_attribute]);
            }
        }
    }
}
