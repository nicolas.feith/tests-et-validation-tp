﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestionPatrimoine
{
    class Borrower
    {
        public Name FirstName => firstName;
        public Name LastName => lastName;
        public List<AttributesAndHabits> Attributes => attributes;
        //attention, l'objet attributes dont on renvoie la référence à son adresse mémoire n'est pas immuable

        private Name firstName;
        private Name lastName;
        private List<AttributesAndHabits> attributes= new List<AttributesAndHabits>();

        public Borrower(Name _firstName, Name _lastName, List<AttributesAndHabits> _attributes)
        {
            firstName = _firstName;
            lastName = _lastName;
            attributes = _attributes;
        }
    }
}
