﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestionPatrimoine
{
    class LoanDuration
    {
        private static readonly uint C_MAX_LEGAL_LOAN_DURATION = 25;
        public uint ValueInYears => valueInYears;
        public uint ValueInMonths => valueInMonths;

        private uint valueInYears;
        private uint valueInMonths;

        public LoanDuration(uint _valueInYears)
        {
            if(_valueInYears <= C_MAX_LEGAL_LOAN_DURATION)
            {
                valueInYears = _valueInYears;
                valueInMonths = _valueInYears * 12;
            }
            else
            {
                valueInYears = C_MAX_LEGAL_LOAN_DURATION;
                valueInMonths = C_MAX_LEGAL_LOAN_DURATION * 12;
            }
        }
    }
}
